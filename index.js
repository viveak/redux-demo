const redux = require('redux')
const reduxLogger = require('redux-logger')

const createStore = redux.createStore
const combineReducers = redux.combineReducers
const applyMiddleware = redux.applyMiddleware
const logger = reduxLogger.createLogger()

const BUY_CAKE = 'BUY_CAKE'
const BUY_ICE_CREAM  = 'BUY_ICE_CREAM'


function buyCake() {
    return {
        type: BUY_CAKE,
        info: 'first redux action '
    }
}
function buyIceCream(){
    return {
        type: BUY_ICE_CREAM,
    }
}

// const initialState = {
//     numOfCakes: 10,
//     numOfIceCream: 20
// }

const initialCake = {
    numOfCakes:10,
}
const initialIceCream = {
    numOfIceCream:20
}

// const reducerCake = (state = initialState, action) => {
//     switch (action.type) {
//         case BUY_CAKE:
//             return {
//                 ...state,
//                 numOfCakes: state.numOfCakes - 1
//             }
//         case BUY_ICE_CREAM:
//             return {
//                 ...state,
//                 numOfIceCream: state.numOfIceCream - 1
//             }
//         default: return state
//     }
// }

const reducerCake = (state = initialCake, action) => {
    switch (action.type) {
        case BUY_CAKE:
            return {
                ...state,
                numOfCakes: state.numOfCakes - 1
            }
        default: return state
    }
}

const reducerIceCream = (state = initialIceCream, action) => {
    switch (action.type) {
        case BUY_ICE_CREAM:
            return {
                ...state,
                numOfIceCream: state.numOfIceCream - 1
            }
        default: return state
    }
}

const rootReducer = combineReducers({
    cake: reducerCake,
    iceCream: reducerIceCream
})

// const store = createStore(reducer)

const store = createStore(rootReducer, applyMiddleware(logger))

console.log('Initial State', store.getState())

const unsubscribe = store.subscribe(()=>{})

store.dispatch(buyCake())
store.dispatch(buyCake())
store.dispatch(buyCake())
store.dispatch(buyIceCream())
store.dispatch(buyIceCream())
unsubscribe()

